provider "aws" {
  region = var.aws_region
}

module "vpc" {
  source = "./modules/vpc"

  env_name     = var.env_name
  cluster_name = var.cluster_name

  cidr            = var.cidr
  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets
  public_subnets_number  = var.public_subnets_number
  private_subnets_number = var.private_subnets_number
}

module "eks" {
  source = "./modules/eks"

  env_name             = var.env_name
  cluster_name         = var.cluster_name
  worker_group_name    = var.worker_group_name
  instance_type        = var.instance_type
  asg_desired_capacity = var.asg_desired_capacity
  asg_max_size         = var.asg_max_size
  asg_min_size         = var.asg_min_size

  vpc_id              = module.vpc.vpc_id
  private_subnets_ids = module.vpc.private_subnets
}