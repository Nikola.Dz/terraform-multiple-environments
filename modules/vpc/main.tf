resource "aws_vpc" "vpc" {
  cidr_block = var.cidr

  enable_dns_hostnames = true

  tags = map(
    "Name", var.env_name,
    "kubernetes.io/cluster/${var.cluster_name}", "shared",
  )
}

resource "aws_subnet" "public_subnet" {
  count = var.public_subnets_number

  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = element(concat(var.public_subnets, [""]), count.index)
  vpc_id            = aws_vpc.vpc.id

  tags = map(
    "Name", "public_subnet_${var.env_name}_${data.aws_availability_zones.available.names[count.index]}",
    "kubernetes.io/cluster/${var.cluster_name}", var.shared_tag,
  )
}

resource "aws_subnet" "private_subnet" {
  count = var.private_subnets_number

  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = element(concat(var.private_subnets, [""]), count.index)
  vpc_id            = aws_vpc.vpc.id

  tags = map(
    "Name", "private_subnet_${var.env_name}_${data.aws_availability_zones.available.names[count.index]}",
    "kubernetes.io/cluster/${var.cluster_name}", var.shared_tag,
  )
}