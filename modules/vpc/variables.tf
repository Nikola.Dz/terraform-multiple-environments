data "aws_availability_zones" "available" {}

variable "cidr" {
  default     = "10.0.0.0/16"
  description = "The CIDR block for the VPC"
  type        = string
}

variable "cluster_name" {
  type        = string
  description = "Name of the EKS cluster. Also used as a prefix in names of related resources."
}

variable "env_name" {
  type        = string
  description = "Environment name"
}

variable "private_subnets" {
  type        = list(string)
  description = "A list of private subnets inside the VPC"
  default     = ["10.0.16.0/22", "10.0.20.0/22", "10.0.24.0/22"]
}

variable "public_subnets" {
  type        = list(string)
  description = "A list of public subnets inside the VPC"
  default     = ["10.0.0.0/22", "10.0.4.0/22", "10.0.8.0/22"]
}

variable "private_subnets_number" {
  type        = number
  description = "Number of private subnets that are created in VPC"
  default     = 2
}

variable "public_subnets_number" {
  type        = number
  description = "Number of public subnets that are created in VPC"
  default     = 2
}

variable "shared_tag" {
  default = "shared"
  type    = string
}