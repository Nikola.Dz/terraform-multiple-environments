variable "asg_desired_capacity" {
  type        = number
  description = "Desired capacity of autoscaling group"
  default     = 2
}

variable "asg_max_size" {
  type        = number
  description = "Max capacity of autoscaling group"
  default     = 2
}

variable "asg_min_size" {
  type        = number
  description = "Min capacity of autoscaling group"
  default     = 2
}

variable "cluster_name" {
  type        = string
  description = "Name of the EKS cluster. Also used as a prefix in names of related resources."
}

variable "env_name" {
  type        = string
  description = "Environment name"
}

variable "instance_type" {
  type        = string
  description = "Instance type of worker groups"
  default     = "t2.medium"
}

variable "private_subnets_ids" {
  type        = list(string)
  description = "A list of subnets to place the EKS cluster and workers within."
}

variable "vpc_id" {
  type        = string
  description = "VPC where the cluster and workers will be deployed."
}

variable "worker_group_name" {
  type        = string
  description = "Worker group name"
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::<AWS_ID>:role/cluster_administrator"
      username = "cluster_administrator"
      groups   = ["system:masters"]
    },
  ]
}