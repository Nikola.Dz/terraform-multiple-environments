module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "13.2.0"

  cluster_name    = var.cluster_name
  cluster_version = "1.18"
  subnets         = var.private_subnets_ids

  tags = {
    Environment = var.env_name
  }

  vpc_id = var.vpc_id

  worker_groups = [
    {
      name                 = var.worker_group_name
      instance_type        = var.instance_type
      asg_desired_capacity = var.asg_desired_capacity
      asg_max_size         = var.asg_max_size
      asg_min_size         = var.asg_min_size
    },
  ]

  map_roles = var.map_roles
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  version = "1.12.0"

  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false

}