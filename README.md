# Terraform multiple environments

Deploying an environment:

``` 
$ cd x_infra
$ terraform init
$ terraform plan
$ terraform apply --auto-approve
```

Variant 2:

```
$ terrafom init
$ terraform plan --var-file="x_infra/terraform.tfvars"
$ terraform apply --var-file="x_infra/terraform.tfvars" --auto-approve
```
