# Environment-related variables
env_name   = "testing_infra"
aws_region = "eu-west-1"

# VPC variables
cidr                   = "10.0.0.0/16"
public_subnets         = ["10.0.0.0/22", "10.0.4.0/22", "10.0.8.0/22"]
private_subnets        = ["10.0.16.0/22", "10.0.20.0/22", "10.0.24.0/22"]
public_subnets_number  = 2
private_subnets_number = 2

# EKS variables
cluster_name         = "testing_infra_cluster"
worker_group_name    = "testing_infra_worker_group"
instance_type        = "t2.small"
asg_desired_capacity = 2
asg_max_size         = 2
asg_min_size         = 2