output "cluster_endpoint" {
  description = "The endpoint for EKS Kubernetes API"
  value       = module.eks.cluster_endpoint
}

output "kubeconfig" {
  description = "kubectl config file contents for this EKS cluster"
  value       = module.eks.kubectl_config
}

output "config_map_aws_auth" {
  description = "A kubernetes configuration to authenticate to this EKS cluster"
  value       = module.eks.config_map_aws_auth
}